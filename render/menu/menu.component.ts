import { Component,trigger, state, style, transition, animate } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router'

import './menu.component.less'

@Component({
  selector: 'eaw-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent {
	public menuState : string

	constructor(private router:Router){
	}
}
