import { NgModule } 		from '@angular/core';
import { BrowserModule }  	from '@angular/platform-browser';
import {NgbModule}    		from '@ng-bootstrap/ng-bootstrap';
import { RouterModule }   from '@angular/router';

import { AppComponent } 	from './app.component';
import { LayoutComponent }   from '../layout/layout.component';
import { HomeComponent }   from '../home/home.component';
import { MenuComponent }   from '../menu/menu.component';


@NgModule({
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      {
        path:'',
        component : HomeComponent
      }
    ])
  ],
  declarations: [
    AppComponent,
    LayoutComponent,
    HomeComponent,
    MenuComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
